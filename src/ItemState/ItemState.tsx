import React from 'react';
import { State } from '../domain/State';

interface Props {
    state: State;
}

export const ItemState = ({ state }: Props) => {
    return (
        <div
            className={`item-state ${state} flex-container centered-container`}
        >
            <div className="column line with-gap"></div>
            <div className="column">
                <span>{state}</span>
            </div>
        </div>
    );
};
