import React, { PropsWithChildren, useCallback, useState } from 'react';

interface Props {
    title: string;
}

export const Accordion = ({ title, children }: PropsWithChildren<Props>) => {
    let [collapsed, setCollapsed] = useState(true);

    let toggleAccordion = useCallback(() => {
        setCollapsed(!collapsed);
    }, [collapsed]);
    return (
        <div className="accordion">
            <div
                className="bold flex-container justified-container"
                onClick={toggleAccordion}
            >
                <span className="column">{title}</span>
                <div className={`column arrow ${collapsed ? 'down' : 'up'}`} />
            </div>
            <hr className="marginT1" />
            {!collapsed && <div className="body">{children}</div>}
        </div>
    );
};
