import Price from './Price';
import { State } from './State';

export default interface Item {
    id: string;
    name: string;
    title: string;
    rate: {
        average: number;
        count: number;
    };
    state: State;
    description: string;
    imagePath: string;
    price: Price;
}
