export default interface Discount {
    value: number;
    isValid: boolean;
}
