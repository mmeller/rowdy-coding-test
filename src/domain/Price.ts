export type Currency = '$';

export default interface Price {
    value: number;
    currency: Currency;
}
