import React from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { BasketDetails } from './BasketDetails/BasketDetails';
import ItemDetails from './Item/ItemDetails';
import { State } from './reducers/rootReducer';

const Application = () => {
    const isLoading = useSelector(
        (state: State) => state.application.isLoading
    );
    return (
        <div>
            {isLoading && (
                <div
                    id="spinner"
                    className=" flex-container centered-container"
                >
                    <span>Loading</span>
                </div>
            )}
            <div id="content">
                <BrowserRouter>
                    <Switch>
                        <Route path="/purchase">
                            <BasketDetails />
                        </Route>
                        <Route path="/">
                            <ItemDetails id={'1'} />
                        </Route>
                    </Switch>
                </BrowserRouter>
            </div>
        </div>
    );
};

export default Application;
