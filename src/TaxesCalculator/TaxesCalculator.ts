import Item from '../domain/Item';
import Price from '../domain/Price';
import ITaxesCalulator from './ITaxesCalculator';

export default class TaxesCalculator implements ITaxesCalulator {
    getTaxes(item: Item): Price {
        return {
            value: parseFloat((item.price.value * 0.19).toFixed(1)),
            currency: item.price.currency,
        };
    }
}
