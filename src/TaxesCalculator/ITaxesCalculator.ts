import Item from '../domain/Item';
import Price from '../domain/Price';

export default interface ITaxesCalulator {
    getTaxes(item: Item): Price;
}
