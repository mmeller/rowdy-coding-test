import ItemBuilder from '../test/builders/ItemBuilder';
import TaxesCalculator from './TaxesCalculator';

describe('TaxesCalculator tests', () => {
    describe('getTaxes tests', () => {
        let itemBuilder: ItemBuilder;

        beforeEach(() => {
            itemBuilder = ItemBuilder.start();
        });

        it('should return 19% with one decimal place', () => {
            const taxesCalculator = new TaxesCalculator();

            const taxes = taxesCalculator.getTaxes(
                itemBuilder.withPrice({ currency: '$', value: 12.99 }).build()
            );

            expect(taxes.value).toEqual(2.5);
        });
    });
});
