import ITaxesCalulator from './ITaxesCalculator';
import TaxesCalculator from './TaxesCalculator';

export default class TaxesCalculatorFactory {
    static instance: ITaxesCalulator = new TaxesCalculator();

    static create() {
        return this.instance;
    }
}
