import IShippingCostService from './IShippingCostService';
import ShippingCostService from './ShippingCostService';

export default class ShippingCostServiceFactory {
    static instance: IShippingCostService = new ShippingCostService();

    static create() {
        return this.instance;
    }
}
