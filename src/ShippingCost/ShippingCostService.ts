import Item from '../domain/Item';
import Price from '../domain/Price';
import IShippingCostService from './IShippingCostService';

export default class ShippingCostService implements IShippingCostService {
    getShippingCost(item: Item): Price {
        return {
            value: 0,
            currency: '$',
        };
    }
}
