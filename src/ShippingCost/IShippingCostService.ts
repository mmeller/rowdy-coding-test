import Item from '../domain/Item';
import Price from '../domain/Price';

export default interface IShippingCostService {
    getShippingCost(item: Item): Price;
}
