import React from 'react';
import reactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Application from './Application';
import { store } from './store';
import './styles/styles.less';

reactDOM.render(
    <Provider store={store}>
        <Application />
    </Provider>,
    document.getElementById('root')
);
