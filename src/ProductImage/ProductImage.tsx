import React from 'react';

interface Props {
    imagePath: string;
    cssClass?: string;
}

export const ProductImage = ({ imagePath, cssClass }: Props) => {
    return (
        <div className="image">
            <img className={cssClass} src={imagePath} alt="Product image" />
        </div>
    );
};
