import Discount from '../domain/Discount';
import IPromoCodeService from './IPromoCodeService';

export default class PromoCodeService implements IPromoCodeService {
    checkPromoCode(promoCode: string): Promise<Discount> {
        return new Promise((resolve) => {
            setTimeout(() => {
                if (promoCode.toUpperCase() === 'DISCOUNT') {
                    resolve({ isValid: true, value: 30 });
                } else {
                    resolve({ isValid: false, value: 0 });
                }
            }, 2000);
        });
    }
}
