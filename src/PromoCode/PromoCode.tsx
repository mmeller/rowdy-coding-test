import React, {
    ChangeEvent,
    ChangeEventHandler,
    useCallback,
    useState,
} from 'react';
import { useDispatch } from 'react-redux';
import { Accordion } from '../Accordion/Accordion';
import { ActionButton } from '../ActionButton/ActionButton';
import { applyPromoCodeOnBasket } from '../reducers/basketReducer';

export const PromoCode = () => {
    let dispatch = useDispatch();
    let [promoCode, setPromoCode] = useState('');

    let changePromoCodeCallback = useCallback(
        (event: ChangeEvent<HTMLInputElement>) => {
            setPromoCode(event.target.value.toUpperCase());
        },
        []
    );
    let applyPromoCode = useCallback(() => {
        dispatch(applyPromoCodeOnBasket(promoCode));
    }, [promoCode]);

    return (
        <div className="promo-code">
            <Accordion title="Apply promo code">
                <div className="flex-container">
                    <div className="column stretch__small-screen with-gap">
                        <input
                            placeholder="Promo code"
                            value={promoCode}
                            onChange={changePromoCodeCallback}
                        />
                    </div>
                    <div className="column stretch">
                        <ActionButton click={applyPromoCode}>
                            Apply
                        </ActionButton>
                    </div>
                </div>
            </Accordion>
        </div>
    );
};
