import IPromoCodeService from './IPromoCodeService';
import PromoCodeService from './PromoCodeService';

export default class PromoCodeServiceFactory {
    static instance: IPromoCodeService = new PromoCodeService();

    static create() {
        return this.instance;
    }
}
