import Discount from '../domain/Discount';

export default interface IPromoCodeService {
    checkPromoCode(promoCode: string): Promise<Discount>;
}
