import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { PriceValue } from './PriceValue';

describe('PriceValue tests', () => {
    it('should show prices with two decimal places', async (done) => {
        render(<PriceValue value={2.5} currency={'$'} />);

        await expect(screen.queryByTestId('value')).toHaveTextContent('2.50');
        done();
    });
});
