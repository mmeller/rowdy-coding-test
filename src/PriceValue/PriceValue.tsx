import React from 'react';
import { Currency } from '../domain/Price';

interface Props {
    value: number;
    currency: Currency;
    valuePrefix?: string;
    valuePostfix?: string;
}

export const PriceValue = ({
    value,
    currency,
    valuePostfix,
    valuePrefix,
}: Props) => {
    return (
        <span>
            {valuePrefix ?? ''}
            <span>{currency}</span>
            <span data-testid="value">{value.toFixed(2)}</span>
            {valuePostfix ?? ''}
        </span>
    );
};
