import Item from '../domain/Item';

export default interface IItemService {
    getItemById(id: string): Promise<Item | undefined>;
}
