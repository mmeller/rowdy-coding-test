import IItemService from './IItemService';
import ItemService from './ItemService';

export class ItemServiceFactory {
    static service: IItemService = new ItemService();
    static create(): IItemService {
        return this.service;
    }
}
