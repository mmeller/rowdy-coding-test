import IItemService from './IItemService';
import Item from '../domain/Item';

const items: Array<Item> = [
    {
        id: '1',
        name: 'Esse commodo proident cupidatat',
        title: 'Ut ex exercitation incididunt',
        rate: {
            average: 4.5,
            count: 33,
        },
        price: {
            value: 12.99,
            currency: '$',
        },
        state: 'clean',
        imagePath: './assets/1.jpg',
        description:
            'Eiusmod non esse amet ut aliquip. Dolor cupidatat aute velit minim excepteur adipisicing aliquip irure culpa. Eu deserunt amet ex ad ea esse. In magna est anim adipisicing dolor dolor occaecat dolore. Pariatur dolore id incididunt dolor Lorem officia. Proident dolore deserunt enim ex reprehenderit dolore ut do labore. Reprehenderit sint dolor reprehenderit minim nostrud amet reprehenderit pariatur qui sit.',
    },
];

export default class ItemService implements IItemService {
    getItemById(id: string): Promise<Item | undefined> {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(items.find((item) => item.id === id));
            }, 2000);
        });
    }
}
