import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Rate } from '../Rate/Rate';
import { changeCurrentItem } from '../reducers/itemReducer';
import { State } from '../reducers/rootReducer';
import { ItemState } from '../ItemState/ItemState';
import { PriceValue } from '../PriceValue/PriceValue';
import { addItemToBasket } from '../reducers/basketReducer';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import ShippingCostServiceFactory from '../ShippingCost/ShippingCostServiceFactory';
import TaxesCalculatorFactory from '../TaxesCalculator/TaxesCalculatorFactory';
import { ProductImage } from '../ProductImage/ProductImage';
import { ActionButton } from '../ActionButton/ActionButton';

interface Props extends RouteComponentProps<any> {
    id: string;
}

const ItemDetails = ({ id, history }: Props) => {
    const shippingCostService = ShippingCostServiceFactory.create();
    const taxesCalculator = TaxesCalculatorFactory.create();

    const dispatch = useDispatch();

    let { currentItem } = useSelector((state: State) => ({
        currentItem: state.item.currentItem,
    }));

    useEffect(() => {
        dispatch(changeCurrentItem(id));
    }, []);

    let purchaseCallback = useCallback(() => {
        if (currentItem) {
            const shippingCost = shippingCostService.getShippingCost(
                currentItem
            );
            const taxes = taxesCalculator.getTaxes(currentItem);
            dispatch(
                addItemToBasket({
                    item: {
                        quantity: 1,
                        item: currentItem,
                    },
                    shippingCost,
                    taxes,
                })
            );
            history.push('/purchase');
        }
    }, [currentItem]);

    return currentItem?.id ? (
        <div className="item-details flex-container centered-container column-container__small-screen">
            <div className="column">
                <ProductImage imagePath={currentItem.imagePath} />
            </div>
            <div className="column">
                <div className="data">
                    <div className="name paddingT1 paddingB1">
                        {currentItem.name}
                    </div>
                    <div className="title bold paddingT1 paddingB1">
                        {currentItem.title}
                    </div>
                    <div className="flex-container justified-container wrap-container paddingT1 invisible__big-screen">
                        <div className="column with-gap">
                            <Rate
                                average={currentItem.rate.average}
                                count={currentItem.rate.count}
                            />
                        </div>
                        <div className="column with-gap">
                            <ItemState state={currentItem.state} />
                        </div>
                    </div>
                    <div className="description">{currentItem.description}</div>
                </div>
                <div className="flex-container centered-container wrap-container__medium-screen justified-container__medium-screen paddingT1">
                    <div className="column with-gap invisible__small-screen">
                        <Rate
                            average={currentItem.rate.average}
                            count={currentItem.rate.count}
                        />
                    </div>
                    <div className="column with-gap invisible__small-screen">
                        <ItemState state={currentItem.state} />
                    </div>
                    <div className="column stretch">
                        <ActionButton click={purchaseCallback}>
                            <React.Fragment>
                                Purchase&bull;
                                <PriceValue
                                    value={currentItem.price.value}
                                    currency={currentItem.price.currency}
                                />
                            </React.Fragment>
                        </ActionButton>
                    </div>
                </div>
            </div>
        </div>
    ) : (
        <div>Item not found</div>
    );
};

export default withRouter(ItemDetails);
