import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { BasketSummary } from './BasketSummary';

describe('BasketSummary tests', () => {
    it('should display Shipping Free if not shipping costs', async (done) => {
        render(<BasketSummary subtotal={0} currency={'$'} />);

        await expect(screen.queryByTestId('shipping')).toHaveTextContent(
            'Free'
        );
        done();
    });

    it('should not display Discount if it is not provided', async (done) => {
        render(<BasketSummary subtotal={0} currency={'$'} />);

        await expect(screen.queryByTestId('discount')).toBeNull();
        done();
    });

    it('should display Discount if it is provided', async (done) => {
        render(
            <BasketSummary
                subtotal={0}
                currency={'$'}
                discount={{ percentage: 30, value: 1 }}
            />
        );

        await expect(screen.queryByTestId('discount')).toHaveTextContent(
            '($1.00)'
        );
        done();
    });
});
