import React from 'react';
import Price, { Currency } from '../domain/Price';
import { PriceValue } from '../PriceValue/PriceValue';

interface Props {
    subtotal: number;
    currency: Currency;
    shippingCost?: Price;
    taxes?: Price;
    discount?: { percentage: number; value: number };
}

export const BasketSummary = ({
    currency,
    shippingCost,
    taxes,
    subtotal,
    discount,
}: Props) => {
    let rows: Array<{
        label: string;
        price?: Price;
        text?: string;
        pricePrefix?: string;
        pricePostfix?: string;
        testId?: string;
    }> = [
        {
            label: 'Subtotal',
            price: {
                value: subtotal,
                currency: currency,
            },
        },
    ];
    if (shippingCost?.value) {
        rows.push({
            label: 'Shipping',
            price: {
                value: shippingCost.value,
                currency: shippingCost.currency,
            },
            testId: 'shipping',
        });
    } else {
        rows.push({
            label: 'Shipping',
            text: 'Free',
            testId: 'shipping',
        });
    }
    if (taxes) {
        rows.push({
            label: 'Taxes & Fees',
            price: {
                value: taxes.value,
                currency: taxes.currency,
            },
        });
    }
    if (discount !== undefined) {
        rows.push({
            label: `Discount(${discount.percentage}%)`,
            price: {
                value: discount.value,
                currency: currency,
            },
            pricePrefix: '(',
            pricePostfix: ')',
            testId: 'discount',
        });
    }

    return (
        <React.Fragment>
            {rows.map(
                (
                    { label, price, text, pricePostfix, pricePrefix, testId },
                    index
                ) => {
                    return (
                        <div className="content flex-container" key={index}>
                            <div className="column stretch">{label}</div>
                            <div className="column" data-testid={testId}>
                                {price ? (
                                    <PriceValue
                                        value={price.value}
                                        currency={price.currency}
                                        valuePostfix={pricePostfix}
                                        valuePrefix={pricePrefix}
                                    />
                                ) : (
                                    <span>{text}</span>
                                )}
                            </div>
                        </div>
                    );
                }
            )}
        </React.Fragment>
    );
};
