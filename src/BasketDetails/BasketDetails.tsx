import React from 'react';
import { useSelector } from 'react-redux';
import { PriceValue } from '../PriceValue/PriceValue';
import { PromoCode } from '../PromoCode/PromoCode';
import { State } from '../reducers/rootReducer';
import { BasketItemDetails } from './BasketItemDetails';
import { BasketSummary } from './BasketSummary';

export const BasketDetails = () => {
    let { items, shippingCost, taxes, discount } = useSelector(
        (state: State) => {
            return {
                items: state.basket.items,
                shippingCost: state.basket.shippingCost,
                taxes: state.basket.taxes,
                discount: state.basket.discount,
            };
        }
    );

    if (items.length > 0) {
        let defaultCurrency = items[0].item.price.currency;
        let subtotal = items
            .map(
                (basketItem) =>
                    basketItem.quantity * basketItem.item.price.value
            )
            .reduce((prev, current) => prev + current, 0);
        let discountValue = discount?.isValid
            ? parseFloat((subtotal * (discount.value / 100)).toFixed(1))
            : 0;

        return (
            <div className="basket-details">
                <div className="page-title bold">Shopping Cart</div>
                <BasketSummary
                    subtotal={subtotal}
                    currency={defaultCurrency}
                    taxes={taxes}
                    discount={
                        discount?.isValid
                            ? {
                                  percentage: discount.value,
                                  value: discountValue,
                              }
                            : undefined
                    }
                />
                <hr className="marginT4" />
                <b>
                    <div className="content flex-container">
                        <div className="column stretch">Total</div>
                        <div className="column">
                            <PriceValue
                                value={
                                    subtotal +
                                    (shippingCost?.value ?? 0) +
                                    (taxes?.value ?? 0) -
                                    discountValue
                                }
                                currency={defaultCurrency}
                            />
                        </div>
                    </div>
                </b>
                {items.map((basketItem, index) => {
                    return (
                        <div className="content" key={index}>
                            <BasketItemDetails
                                item={basketItem.item}
                                quantity={basketItem.quantity}
                            />
                        </div>
                    );
                })}
                <div className="content">
                    <PromoCode />
                </div>
            </div>
        );
    } else {
        return (
            <div className="basket-details">
                <span className="marginR1">Basket is empty.</span>
                <a href="/">Go to main page</a>
            </div>
        );
    }
};
