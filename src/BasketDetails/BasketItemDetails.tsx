import React from 'react';
import { Accordion } from '../Accordion/Accordion';
import Item from '../domain/Item';
import { PriceValue } from '../PriceValue/PriceValue';
import { ProductImage } from '../ProductImage/ProductImage';

interface Props {
    item: Item;
    quantity: number;
}

export const BasketItemDetails = ({ item, quantity }: Props) => {
    return (
        <Accordion title="See item details">
            <div className="flex-container">
                <div className="column">
                    <ProductImage
                        imagePath={item.imagePath}
                        cssClass="small mini__small-screen"
                    />
                </div>
                <div className="column flex-container column-container vertical-aligned-container">
                    <div className="bold paddingB1">{item.title}</div>
                    <div className="bold paddingB1">
                        <PriceValue
                            value={item.price.value}
                            currency={item.price.currency}
                        />
                    </div>
                    <div className="paddingB1">Qty.: {quantity}</div>
                </div>
            </div>
        </Accordion>
    );
};
