import { createAction, createReducer } from '@reduxjs/toolkit';

const START_LOADING = 'START_LOADING';
const STOP_LOADING = 'STOP_LOADING';

export interface ApplicationState {
    isLoading: boolean;
}

const initialState: ApplicationState = {
    isLoading: false,
};

export const startLoading = createAction(START_LOADING);
export const stopLoading = createAction(STOP_LOADING);

export const applicationReducer = createReducer(initialState, (builder) => {
    builder
        .addCase(startLoading, (state) => {
            return {
                ...state,
                isLoading: true,
            };
        })
        .addCase(stopLoading, (state) => {
            return {
                ...state,
                isLoading: false,
            };
        });
});
