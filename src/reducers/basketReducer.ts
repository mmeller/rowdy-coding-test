import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import Discount from '../domain/Discount';
import Item from '../domain/Item';
import Price from '../domain/Price';
import IPromoCodeService from '../PromoCode/IPromoCodeService';
import PromoCodeServiceFactory from '../PromoCode/PromoCodeServiceFactory';
import { startLoading, stopLoading } from './applicationReducer';

export const ADD_ITEM_TO_BASKET = 'ADD_ITEM_TO_BASKET';

export interface BasketItem {
    quantity: number;
    item: Item;
}

export interface BasketState {
    items: Array<BasketItem>;
    shippingCost?: Price;
    taxes?: Price;
    discount?: Discount;
}

const initialState: BasketState = {
    items: [],
    shippingCost: undefined,
    taxes: undefined,
    discount: undefined,
};

interface AddItemToBasketActionType {
    item: BasketItem;
    shippingCost: Price;
    taxes: Price;
}

export const basketReducer = createSlice({
    name: 'basket',
    initialState,
    reducers: {
        addItemToBasket: (
            state,
            action: PayloadAction<AddItemToBasketActionType>
        ) => {
            return {
                ...state,
                items: [...state.items, action.payload.item],
                shippingCost: action.payload.shippingCost,
                taxes: action.payload.taxes,
            };
        },
        applyDiscount: (state, action: PayloadAction<Discount>) => {
            return {
                ...state,
                discount: action.payload,
            };
        },
    },
});

export const { addItemToBasket, applyDiscount } = basketReducer.actions;

export const applyPromoCodeOnBasket = createAsyncThunk(
    ADD_ITEM_TO_BASKET,
    async (promoCode: string, { dispatch }) => {
        dispatch(startLoading());

        const service: IPromoCodeService = PromoCodeServiceFactory.create();
        const discount = await service.checkPromoCode(promoCode);

        dispatch(applyDiscount(discount));
        dispatch(stopLoading());
    }
);
