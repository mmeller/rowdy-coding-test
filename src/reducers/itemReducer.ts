import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import Item from '../domain/Item';
import { ItemServiceFactory } from '../Item/ItemServiceFactory';
import { startLoading, stopLoading } from './applicationReducer';

const CHANGE_CURRENT_ITEM = 'CHANGE_CURRENT_ITEM';

export interface ChangeCurrentItemType {
    id: string;
}

export interface ItemState {
    currentItem?: Item;
}

const initItemState: ItemState = {
    currentItem: undefined,
};

export const itemReducer = createSlice({
    name: 'item',
    initialState: initItemState,
    reducers: {
        itemLoaded(state, action) {
            return {
                ...state,
                currentItem: {
                    ...action.payload,
                },
            };
        },
    },
});

const { itemLoaded } = itemReducer.actions;

export const changeCurrentItem = createAsyncThunk(
    CHANGE_CURRENT_ITEM,
    async (id: string, { dispatch }) => {
        dispatch(startLoading());
        const itemService = ItemServiceFactory.create();
        const item = await itemService.getItemById(id);
        dispatch(itemLoaded(item));
        dispatch(stopLoading());
    }
);
