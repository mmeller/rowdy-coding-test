import { combineReducers } from 'redux';
import { applicationReducer, ApplicationState } from './applicationReducer';
import { basketReducer, BasketState } from './basketReducer';
import { itemReducer, ItemState } from './itemReducer';

export interface State {
    item: ItemState;
    basket: BasketState;
    application: ApplicationState;
}

export const rootReducer = combineReducers({
    item: itemReducer.reducer,
    basket: basketReducer.reducer,
    application: applicationReducer,
});
