import React, { PropsWithChildren } from 'react';

interface Props {
    click: () => void;
}

export const ActionButton = ({ click, children }: PropsWithChildren<Props>) => {
    return (
        <div
            className="action-button flex-container centered-container"
            onClick={click}
        >
            {children}
        </div>
    );
};
