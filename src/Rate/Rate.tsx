import React, { CSSProperties } from 'react';

interface Props {
    average: number;
    count: number;
}

export const Rate = (props: Props) => {
    var ratingStyle = { '--rating': props.average } as CSSProperties;
    return (
        <div className="rate">
            <div className="stars" style={ratingStyle}>
                <span>{props.count}</span>
            </div>
        </div>
    );
};
