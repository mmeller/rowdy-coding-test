import Item from '../../domain/Item';
import Price from '../../domain/Price';

export default class ItemBuilder {
    private constructor(private price?: Price) {}

    withPrice(price: Price): ItemBuilder {
        return new ItemBuilder(price);
    }

    build(): Item {
        return {
            id: '',
            description: '',
            imagePath: '',
            name: '',
            price: this.price ?? {
                value: 0,
                currency: '$',
            },
            rate: {
                average: 0,
                count: 0,
            },
            state: 'clean',
            title: '',
        };
    }

    static start(): ItemBuilder {
        return new ItemBuilder();
    }
}
